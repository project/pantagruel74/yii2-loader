<?php

namespace Pantagruel74\Yii2LoaderTestUnit;

use Pantagruel74\Yii2Loader\Yii2Loader;
use PHPUnit\Framework\TestCase;

class Yii2LoaderTest extends TestCase
{
    public function testLoad(): void
    {
        Yii2Loader::load();
        $this->assertTrue(class_exists("\Yii"));
    }
}