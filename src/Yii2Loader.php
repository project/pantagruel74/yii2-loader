<?php

namespace Pantagruel74\Yii2Loader;

use Webmozart\Assert\Assert;

class Yii2Loader
{
    /**
     * @param string|null $startDir
     * @return void
     */
    public static function load(?string $startDir = null): void
    {
        $ds = DIRECTORY_SEPARATOR;
        if(!isset($startDir)) {
            $startDir = __DIR__;
        }
        $startDir = self::removeLastCharDirectorySeparator($startDir);
        $yiiPath = static::searchYiiDir($startDir);
        require_once $yiiPath;
        Assert::classExists('\Yii');
    }

    /**
     * @param string $startDir
     * @return string
     */
    protected static function searchYiiDir(string $startDir): string
    {
        $ds = DIRECTORY_SEPARATOR;
        $startDir = str_replace("/", $ds, $startDir);
        $searchResult = null;
        for($dirParts = explode($ds, $startDir); count($dirParts) > 0; array_pop($dirParts))
        {
            $searchResult = self::checkVendorOfYii2(implode($ds, $dirParts));
            if(!is_null($searchResult)) {
                break;
            }
        }
        Assert::notNull($searchResult, 'Cant find Yii class');
        return $searchResult;
    }

    /**
     * @param string $startDir
     * @return string
     */
    protected static function removeLastCharDirectorySeparator(string $startDir): string
    {
        $ds = DIRECTORY_SEPARATOR;
        $dirParts = explode($ds, $startDir);
        if(empty(end($dirParts))) {
            array_pop($dirParts);
        }
        return implode($ds, $dirParts);
    }

    /**
     * @param string $startDir
     * @return string|null
     */
    protected static function checkVendorOfYii2(string $startDir): ?string
    {
        $ds = DIRECTORY_SEPARATOR;
        if(
            (is_dir($startDir . $ds . 'vendor'))
            && is_dir($startDir . $ds . 'vendor' . $ds . "yiisoft")
            && is_dir($startDir . $ds . 'vendor' . $ds . "yiisoft" . $ds . "yii2")
            && file_exists($startDir . $ds . 'vendor' . $ds . "yiisoft" . $ds . "yii2" . $ds . "Yii.php")
        ) {
            return $startDir . $ds . 'vendor' . $ds . "yiisoft" . $ds . "yii2" . $ds . "Yii.php";
        }
        return null;
    }
}